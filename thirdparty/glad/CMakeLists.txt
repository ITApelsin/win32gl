cmake_minimum_required(VERSION 3.17)

file(GLOB SRC "src/*.c")

set(GLAD_INCLUDE "${CMAKE_CURRENT_SOURCE_DIR}/include" PARENT_SCOPE)

add_library(glad STATIC ${SRC})
target_include_directories(glad PRIVATE include)
