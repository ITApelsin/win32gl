# Win32GL
## About
OpenGL 12 with Win32 API
## Technologies
- Win32 API
- OpenGL 12
- CMake
- VC++2019

## Project Structure
- /data - runtime data
- /data/shaders - shaders sources
- /data/textures - project textures
- /ogl - object OpenGL
- /src - project sources
- /thirdparty - thirdparty libraries
- /build - build directory
- /build/out/$Config - out runtime directory
