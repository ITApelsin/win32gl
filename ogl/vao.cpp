#include "ogl/vao.h"

namespace ogl
{
    GLVAO::GLVAO(GLVAO&& move) noexcept : uid(move.uid)
    {
        move.uid = 0;
    }

    GLVAO& GLVAO::operator=(GLVAO&& move) noexcept 
    {
        if (&move == this)
        {
            return *this;
        }

        uid = move.uid;
        move.uid = 0;
        return *this;
    }

    GLVAO::GLVAO()
    {
        glGenVertexArrays(1, &uid);
    }

    GLVAO::~GLVAO()
    {
        if (uid)
        {
            glDeleteVertexArrays(1, &uid);
        }
    }

    void GLVAO::bind()
    {
        glBindVertexArray(uid);
    }

    GLVAO:: operator GLuint() const
    {
        return uid;
    }
}
