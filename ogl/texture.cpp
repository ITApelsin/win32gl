#include <SOIL/SOIL.h>
#include "ogl/texture.h"

namespace ogl 
{
    GLTEXTURE::GLTEXTURE()
    {
        glGenTextures(1, &uid);
        bind(GL_TEXTURE_2D);
        texWrap(GL_REPEAT);
        texFilter(GL_LINEAR);
    }

    GLTEXTURE::~GLTEXTURE()
    {
        if (uid)
        {
            glDeleteTextures(1, &uid);
        }
    }

    GLTEXTURE::GLTEXTURE(GLTEXTURE&& move) noexcept : uid(move.uid)
    {
        move.uid = 0;
    }

    GLTEXTURE& GLTEXTURE::operator=(GLTEXTURE&& move) noexcept
    {
        if (&move == this)
        {
            return *this;
        }
        uid = move.uid;
        move.uid = 0;
        return *this;
    }

    GLTEXTURE::operator GLuint() const
    {
        return uid;
    }

    void GLTEXTURE::bind(GLenum target)
    {
        glBindTexture(target, uid);
    }

    void GLTEXTURE::texParameteri(GLenum pname, GLint param, GLenum target)
    {
        bind(target);
        glTexParameteri(target, pname, param);
    }

    void GLTEXTURE::texWrap(GLint s, GLint t, GLenum target)
    {
        texParameteri(GL_TEXTURE_WRAP_S, s, target);
        texParameteri(GL_TEXTURE_WRAP_T, t, target);
    }

    void GLTEXTURE::texWrap(GLint st, GLenum target)
    {
        texWrap(st, st, target);
    }

    void GLTEXTURE::texFilter(GLint minFilter, GLint magFilter, GLenum target) 
    {
        texParameteri(GL_TEXTURE_MIN_FILTER, minFilter, target);
        texParameteri(GL_TEXTURE_MAG_FILTER, magFilter, target);
    }

    void GLTEXTURE::texFilter(GLint minMag, GLenum target) 
    {
        texFilter(minMag, minMag, target);
    }

    void GLTEXTURE::generateMipmap(GLenum target)
    {
        bind(target);
        glGenerateMipmap(target);
    }

    void GLTEXTURE::texImage2d(const void *image, int width, int height, GLenum format, GLenum target, GLint border, GLint level)
    {

        bind(target);
        glTexImage2D(target, level, format, width, height, border, format, GL_UNSIGNED_BYTE, image);
    }

    bool GLTEXTURE::texImage2d(const char* path, GLenum format, GLenum target, GLint border, GLint level)
    {

        int texWidth, texHeight;
        unsigned char* image = SOIL_load_image(path, &texWidth, &texHeight, 0, SOIL_LOAD_RGB);
        if (image)
        {
            texImage2d(reinterpret_cast<const void*>(image), texWidth, texHeight, format, target, border, level);
            SOIL_free_image_data(image);
            return true;
        }
        else
        {
            return false;
        }
    }
}
