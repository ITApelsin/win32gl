#ifndef GL_PROGRAM_H
#define GL_PROGRAM_H

#include <glad/glad.h>
#include <string>

#include "ogl/glshader.h"

namespace ogl
{
    class GLProgram
    {
    private:
        GLuint uid;
    public:
        // no copy
        GLProgram(GLProgram& copy) = delete;
        GLProgram& operator=(GLProgram& copy) = delete;

        // may be moved
        GLProgram(GLProgram&& move) noexcept;
        GLProgram& operator=(GLProgram&& move) noexcept;

        GLProgram();
        ~GLProgram();

        operator GLuint() const;

        bool link(GLShader **shaders);
        void use();
        std::string getErrorLog();
        GLint getUniformLocation(const char *name);
        
        void uniform1i(GLint location, GLint value);
        void uniform1i(const char *name, GLint value);
    };
}

#endif // GL_PROGRAM_H
