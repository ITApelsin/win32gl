#ifndef OGL_TEXTURE_H
#define OGL_TEXTURE_H

#include <glad/glad.h>

namespace ogl 
{
    class GLTEXTURE 
    {
        GLuint uid;
    public:
        // no copy
        GLTEXTURE(GLTEXTURE& copy) = delete;
        GLTEXTURE& operator=(GLTEXTURE& copy) = delete;

        // may be moved
        GLTEXTURE(GLTEXTURE&& move) noexcept;
        GLTEXTURE& operator=(GLTEXTURE&& move) noexcept;
        
        GLTEXTURE();
        ~GLTEXTURE();

        operator GLuint() const;

        void bind(GLenum target);
        void texParameteri(GLenum pname, GLint param, GLenum target = GL_TEXTURE_2D);

        void texWrap(GLint s, GLint t, GLenum target = GL_TEXTURE_2D);
        void texWrap(GLint st, GLenum target = GL_TEXTURE_2D);

        void texFilter(GLint minFilter, GLint magFilter, GLenum target = GL_TEXTURE_2D);
        void texFilter(GLint minMag, GLenum target = GL_TEXTURE_2D);

        void generateMipmap(GLenum target = GL_TEXTURE_2D);

        void texImage2d(const void *image, int width, int height, GLenum format = GL_RGB, GLenum target = GL_TEXTURE_2D, GLint border = 0, GLint level = 0);
        bool texImage2d(const char *path, GLenum format = GL_RGB, GLenum target = GL_TEXTURE_2D, GLint border = 0, GLint level = 0);
    };
}

#endif // OGL_TEXTURE_H
