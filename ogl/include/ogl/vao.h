#ifndef OGL_VAO_H
#define OGL_VAO_H

#include <glad/glad.h>

namespace ogl
{
    class GLVAO
    {
        GLuint uid;
    public:
        // no copy
        GLVAO(GLVAO& copy) = delete;
        GLVAO& operator=(GLVAO& copy) = delete;

        // can move
        GLVAO(GLVAO&& move) noexcept;
        GLVAO& operator=(GLVAO&& move) noexcept;

        GLVAO();
        ~GLVAO();

        void bind();
        operator GLuint() const;
    };
}

#endif // OGL_VAO_H
