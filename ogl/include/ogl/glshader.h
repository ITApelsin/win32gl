#ifndef GL_SHADER_H
#define GL_SHADER_H

#include <glad/glad.h>
#include <string>

namespace ogl
{
    class GLShader 
    {
    private:
        GLuint uid;
    public:
        // no copy
        GLShader(GLShader &copy) = delete;
        GLShader& operator=(GLShader &copy) = delete;

        // can move
        GLShader(GLShader &&move) noexcept;
        GLShader& operator=(GLShader &&move) noexcept;

        GLShader(GLenum shaderType);
        ~GLShader();
        int compile(int count, const char**strings);
        int compile(std::string shaderSource);
        std::string getErrorLog();
        operator GLuint() const;
    };
}

#endif // GL_SHADER_H
