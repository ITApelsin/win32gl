#ifndef OGL_VBO_H
#define OGL_VBO_H

#include <glad/glad.h>

namespace ogl 
{
    class GLVBO
    {
        GLuint uid;
    public:
        // no copy
        GLVBO(GLVBO& copy) = delete;
        GLVBO& operator=(GLVBO& copy) = delete;

        // can move
        GLVBO(GLVBO&& move) noexcept;
        GLVBO& operator=(GLVBO&& move) noexcept;

        GLVBO();
        ~GLVBO();
        operator GLuint() const;
        void bind(GLenum target);
    };
}

#endif // OGL_VBO_H
