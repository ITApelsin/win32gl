#include "ogl/vbo.h"

namespace ogl 
{
    GLVBO::GLVBO(GLVBO&& move) noexcept : uid(move.uid)
    {
        move.uid = 0;
    }

    GLVBO& GLVBO::operator=(GLVBO&& move) noexcept
    {
        if (&move == this)
        {
            return *this;
        }

        uid = move.uid;
        move.uid = 0;
        return *this;
    }

    GLVBO::GLVBO() 
    {
        glGenBuffers(1, &uid);
    }

    GLVBO::~GLVBO() 
    {
        glDeleteBuffers(1, &uid);
    }

    GLVBO::operator GLuint() const
    {
        return uid;
    }

    void GLVBO::bind(GLenum target)
    {
        glBindBuffer(target, uid);
    }
}
