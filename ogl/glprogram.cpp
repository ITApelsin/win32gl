#include <vector>
#include "ogl/glprogram.h"

using namespace std;

namespace ogl
{

    GLProgram::GLProgram() 
    { 
        uid = glCreateProgram();
    }

    GLProgram::~GLProgram()
    {
        if (uid) 
        {
            glDeleteProgram(uid);
        }
    }

    GLProgram::GLProgram(GLProgram  &&move) noexcept : uid(move.uid)
    {
        move.uid = NULL;
    }

    GLProgram& GLProgram::operator=(GLProgram&& move) noexcept
    {
        if (&move == this) 
        {
            return *this;
        }

        uid = move.uid;
        move.uid = NULL;
        return *this;
    }

    GLProgram::operator GLuint() const
    {
        return uid;
    }

    bool GLProgram::link(GLShader** shaders)
    {
        for (GLShader **pSh = shaders; NULL != *pSh; pSh++) 
        {
            glAttachShader(uid, static_cast<GLuint>(*(*pSh)));
        }
        glLinkProgram(uid);
        for (GLShader **pSh = shaders; NULL != *pSh; pSh++) 
        {
            glDetachShader(uid, static_cast<GLuint>(*(*pSh)));
        }
        GLint status;
        glGetProgramiv(uid, GL_LINK_STATUS, &status);
        return status;
    }

    void GLProgram::use()
    {
        glUseProgram(uid);
    }

    string GLProgram::getErrorLog()
    {
        int maxLength = 0;
        glGetProgramiv(uid, GL_INFO_LOG_LENGTH, &maxLength);
        if (!maxLength) 
        {
            return "";
        }

        vector<GLchar> errorLog(maxLength);
        glGetProgramInfoLog(uid, maxLength, &maxLength, &errorLog[0]);
        return string(&errorLog[0]);
    }

    GLint GLProgram::getUniformLocation(const char *name)
    {
        return glGetUniformLocation(uid, name);
    }

    void GLProgram::uniform1i(GLint location, GLint value)
    {
        glUniform1i(location, value);
    }

    void GLProgram::uniform1i(const char *name, GLint value)
    {
        this->use();
        this->uniform1i(this->getUniformLocation(name), value);
    }
}
