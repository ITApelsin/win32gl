#include <vector>
#include "ogl/glshader.h"

using namespace std;

namespace ogl 
{
    GLShader::GLShader(GLenum a_shaderType)
    {
        uid = glCreateShader(a_shaderType);
    }

    GLShader::~GLShader()
    {
        if (uid) 
        {
            glDeleteShader(uid);
        }
    }

    GLShader::GLShader(GLShader &&move) noexcept : uid(move.uid)
    {
        move.uid = NULL;
    } 

    GLShader& GLShader::operator=(GLShader &&move) noexcept
    {
        if (&move == this) 
        {
            return *this;
        }

        uid = move.uid;
        move.uid = NULL;
        return *this;
    }

    int GLShader::compile(int count, const char **strings)
    {
        glShaderSource(uid, count, strings, NULL);
        glCompileShader(uid);
        int result;
        glGetShaderiv(uid, GL_COMPILE_STATUS, &result);
        return result;
    }

    int GLShader::compile(string a_shaderSource) 
    {
        const char* shaderSource = a_shaderSource.c_str();
        return compile(1, &shaderSource);
    }

    string GLShader::getErrorLog()
    {
        int maxLength = 0;
        glGetShaderiv(uid, GL_INFO_LOG_LENGTH, &maxLength);
        
        vector<GLchar> errorLog(maxLength);
        glGetShaderInfoLog(uid, maxLength, &maxLength, &errorLog[0]);
        return string(&errorLog[0]);
    }

    GLShader::operator GLuint() const
    {
        return uid;
    }
}
