cmake_minimum_required(VERSION 3.17)
project(Win32GL VERSION 1.0)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED TRUE)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/out")

add_custom_target(copy-resources ALL
    COMMAND cmake -E copy_directory 
    "${CMAKE_CURRENT_SOURCE_DIR}/data" 
	"${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/$<CONFIG>/data")

add_subdirectory(thirdparty/glad)
add_subdirectory(thirdparty/glm)
add_subdirectory(thirdparty/soil)
add_subdirectory(ogl)
add_subdirectory(src)
