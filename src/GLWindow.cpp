#define GLEW_STATIC

#include <glad/glad.h>
#include <glad/glad_wgl.h>
#include <windows.h>

#include "glwindow.h"
#include "render.h"

#define MESSAGE(msg) MessageBox(NULL, msg,"SHUTDOWN ERROR",MB_OK | MB_ICONSTOP)
#define WND_CLASS_NAME "GLWND_CLS"

static const PIXELFORMATDESCRIPTOR pfd =
{
    sizeof(PIXELFORMATDESCRIPTOR),
    1,
    PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,
    PFD_TYPE_RGBA,              
    32, 
    0, 0, 0, 0, 0, 0,
    0,
    0,
    0,
    0, 0, 0, 0,
    24,
    8,
    0,
    PFD_MAIN_PLANE,
    0,
    0, 0, 0
};

static const int context_attributes[] = {
    WGL_CONTEXT_MAJOR_VERSION_ARB, 4,
    WGL_CONTEXT_MINOR_VERSION_ARB, 4,
    WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
    0
};

static bool g_wndClassRegistered = false;
static bool g_contextInitialized = false;

static HWND g_hWnd = NULL;
static HINSTANCE g_hInstance = NULL;
static HGLRC g_hGLRC = NULL;
static HDC g_hDC = NULL;

LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
        case WM_CLOSE:
        {
            PostQuitMessage(0);
            return 0;
        }
        case WM_SIZE:
        {
            if (g_contextInitialized) 
            {
                int width = LOWORD(lParam);
                int height = HIWORD(lParam);
                if (0 == height)
                {
                    height = 1;
                }
                onResize(width, height);
            }
            return 0;
        }
    }
    return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

bool registerWndClass(HINSTANCE hInstance)
{
    if (g_wndClassRegistered)
    {
        return true;
    }

    WNDCLASS wc;
    wc.style            = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wc.lpfnWndProc      = (WNDPROC) WndProc;
    wc.cbClsExtra       = 0;
    wc.cbWndExtra       = 0;
    wc.hInstance        = hInstance;
    wc.hIcon            = LoadIcon(NULL, IDI_WINLOGO);
    wc.hCursor          = LoadCursor(NULL, IDC_ARROW); 
    wc.hbrBackground    = NULL;
    wc.lpszMenuName     = NULL;
    wc.lpszClassName    = WND_CLASS_NAME;

    if (!RegisterClass(&wc))
    {
        MESSAGE("Failed to register window class");
        return false;
    }
    g_wndClassRegistered = true;
    return true;
}

HGLRC initGLContext(HWND hWnd, HDC hDC) 
{
    int pixelFormat = ChoosePixelFormat(hDC, &pfd);
    if (!pixelFormat) 
    {
        MESSAGE("Can't find sutable pixel format");
        return nullptr;
    }
    if (!SetPixelFormat(hDC, pixelFormat, &pfd)) 
    {
        MESSAGE("Can't set choosen pixel format");
        return nullptr;
    }

    const HGLRC tempContext = wglCreateContext(hDC);
    wglMakeCurrent(hDC, tempContext);

    int wglInitErr = gladLoadWGL(hDC);
    int glInitErr = gladLoadGL();

    if (!wglInitErr || !glInitErr)
    {
        MESSAGE("Can't init glad");
        wglMakeCurrent(NULL, NULL);
        wglDeleteContext(tempContext);
        return nullptr;
    }

    HGLRC glContext = wglCreateContextAttribsARB(hDC, NULL, context_attributes);
    wglMakeCurrent(NULL, NULL);
    wglDeleteContext(tempContext);
    return glContext;
}

bool glWnd_open(HINSTANCE hInstance, WNDOPTIONS opt)
{
    if (!registerWndClass(hInstance)) 
    {
        return false;
    }
    
    RECT wndRect;
    wndRect.left = (long) 0;
    wndRect.right = (long) opt.width;
    wndRect.top = (long) 0;
    wndRect.bottom = (long) opt.height;

    DWORD dwExStyle = 0;
    DWORD dwStyle = 0;

    dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
    dwStyle = WS_OVERLAPPEDWINDOW;

    AdjustWindowRectEx(&wndRect, dwStyle, FALSE, dwExStyle);

    HWND l_hWnd;
    if (!(l_hWnd = CreateWindowEx(
                    dwExStyle,
                    WND_CLASS_NAME,
                    opt.title,
                    WS_CLIPSIBLINGS | WS_CLIPCHILDREN | dwStyle, 
                    NULL, NULL, 
                    wndRect.right - wndRect.left,
                    wndRect.bottom - wndRect.top,
                    NULL, NULL,
                    hInstance,
                    NULL)))
    {
        MESSAGE("Window creation error");
        return false;
    }

    HDC l_hDC = GetDC(l_hWnd);
    if (!l_hDC) 
    {
        MESSAGE("Can't accept window descriptor");
        DestroyWindow(l_hWnd);
        return false;
    }

    HGLRC l_glContext;

    if (!(l_glContext = initGLContext(l_hWnd, l_hDC))) 
    {
        MESSAGE("Can't create GL context");
        ReleaseDC(l_hWnd, l_hDC);
        DestroyWindow(l_hWnd);
        return false;
    }

    wglMakeCurrent(l_hDC, l_glContext);
    onCreate();
    /*if (!onCreate(width, height)) 
    {
        MESSAGE("Can't initialize gl context");
        wglMakeCurrent(NULL, NULL);
        wglDeleteContext(l_glContext);
        ReleaseDC(l_hWnd, l_hDC);
        DestroyWindow(l_hWnd);
        return false;
    }*/

    g_hWnd = l_hWnd;
    g_hInstance = hInstance;
    g_hGLRC = l_glContext;
    g_hDC = l_hDC;
    g_contextInitialized = true;

    ShowWindow(g_hWnd, SW_SHOW);
    glWnd_refresh();

    return true;
}

void glWnd_close()
{
    onDestroy();
    wglMakeCurrent(NULL, NULL);
    wglDeleteContext(g_hGLRC);
    ReleaseDC(g_hWnd, g_hDC);
    DestroyWindow(g_hWnd);
}

void glWnd_refresh() 
{
    onDraw();
    SwapBuffers(g_hDC);
}
