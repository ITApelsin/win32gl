#include <fstream>
#include <sstream>
#include "ioutils.h"

using namespace std;

bool readAsText(string &dest, const char* path)
{
    stringstream strs;
    fstream file(path);
    if (file.is_open())
    {
        std::string line;
        while (std::getline(file, line))
        {
            strs << line << '\n';
        }
        dest = strs.str();
        file.close();
        return true;
    }
    return false;
}
