#include <windows.h>
#include "glwindow.h"
#define FPS 30

static const DWORD FRAME_DUR = 1000 / FPS;

int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	if (!glWnd_open(hInstance, WNDOPTIONS{ 640, 480, "glwindow" }))
	{
		return -1;
	}
	MSG msg = { 0 };
	while (msg.message != WM_QUIT) 
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		glWnd_refresh();
		// calc for each frame
		Sleep(FRAME_DUR);
	}
	glWnd_close();
	return 0;
}
