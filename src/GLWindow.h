#ifndef GL_WINDOW_H
#define GL_WINDOW_H

struct WNDOPTIONS
{
    int width;
    int height;
    char* title;
};

bool glWnd_open(HINSTANCE hInstance, WNDOPTIONS opt);

void glWnd_refresh();

void glWnd_close();

#endif // GL_WINDOW_H
