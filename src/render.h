#ifndef GL_RENDER_H
#define GL_RENDER_H

void onCreate();
void onDestroy();
void onResize(int width, int height);
void onDraw();

#endif // GL_RENDER_H
