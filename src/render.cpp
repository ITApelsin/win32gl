#include "render.h"
#include <glad/glad.h>
#include <ogl/glprogram.h>
#include <ogl/glshader.h>
#include <ogl/texture.h>
#include "ioutils.h"
#include <vector>
#include <Windows.h>
#include <memory>
#include <shlobj.h>
#include <SOIL/SOIL.h>

#define MESSAGE(msg) MessageBox(NULL, msg,"SHUTDOWN ERROR",MB_OK | MB_ICONSTOP)

using namespace std;
using namespace ogl;

static const GLfloat g_vVertices[] = 
{
	-0.5f, -0.5f, 0.0f,     0.0f, 0.0f,
	 0.5f, -0.5f, 0.0f,     1.0f, 0.0f,
	 0.0f,  0.5f, 0.0f,     0.5f, 1.0f
};

unique_ptr<GLProgram> g_glProgram;
unique_ptr<GLTEXTURE> g_texture;

GLuint VBO;
GLuint VAO;


void onCreate()
{
	string vShaderSource;
	string fShaderSource;

	if (!readAsText(vShaderSource, "data/shaders/vertex_shader.glsl")
		|| !readAsText(fShaderSource, "data/shaders/fragment_shader.glsl"))
	{
		MESSAGE("can't read shaders' sources");
		PostQuitMessage(-1);
		return;
	}

	GLShader vShader(GL_VERTEX_SHADER);
	GLShader fShader(GL_FRAGMENT_SHADER);

	if (!vShader.compile(vShaderSource) || !fShader.compile(fShaderSource))
	{
		MESSAGE("can't compile any shaders");
		PostQuitMessage(-1);
		return;
	}

	GLShader* shaders[] = { &vShader, &fShader, NULL };

	GLProgram* l_glProgram = new GLProgram();

	if (!l_glProgram->link(shaders)) 
	{
		MESSAGE("can't link program");
		delete l_glProgram;
		PostQuitMessage(-1);
		return;
	}

	g_glProgram.reset(l_glProgram);

	glGenBuffers(1, &VBO);
	glGenVertexArrays(1, &VAO);

	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(g_vVertices), g_vVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*) (3 * sizeof(GLfloat)));

	glBindVertexArray(0);

    int texWidth, texHeight;
    unsigned char* image = SOIL_load_image("data/textures/stone.jpg", &texWidth, &texHeight, 0, SOIL_LOAD_RGB);

	GLTEXTURE* l_texture = new GLTEXTURE();

	if (!l_texture->texImage2d("data/textures/stone.jpg"))
	{
		// TODO destroy gl objects
		return;
	}

	g_texture.reset(l_texture);
        
    glActiveTexture(GL_TEXTURE1);
	g_texture->bind(GL_TEXTURE_2D);
    glActiveTexture(GL_TEXTURE0);

	g_glProgram->uniform1i("textureSampler", 0);
}

void onDestroy()
{
	g_glProgram.reset(nullptr);
}

void onResize(int width, int height)
{
	glViewport(0, 0, width, height);
}

void onDraw()
{
	glClearColor(0.5f, 0.5f, 0.5f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	g_glProgram->use();
	glBindVertexArray(VAO);
	glDrawArrays(GL_TRIANGLES, 0, 3);
	glBindVertexArray(0);
}
