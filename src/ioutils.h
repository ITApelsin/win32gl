#ifndef IO_UTILS_H
#define IO_UTILS_H

#include <string>

bool readAsText(std::string &dest, const char *path);

#endif // IO_UTILS_H
