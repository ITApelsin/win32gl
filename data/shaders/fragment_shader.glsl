#version 440 core

precision mediump float;

const float zero = 0.0;
const float one = 1.0;

uniform sampler2D textureSmapler;

in vec2 fTexCoord;

out vec4 color;

void main()
{
    color = texture(textureSmapler, fTexCoord);
}
