#version 440 core

#define POS_LOC 0
#define TEX_COORD_LOC 1

layout(location = POS_LOC) in vec3 vPosition;
layout(location = TEX_COORD_LOC)in vec2 vTexCoord;

out vec2 fTexCoord;

void main() 
{
    gl_Position = vec4(vPosition, 1.0);
    fTexCoord = vTexCoord;
}
